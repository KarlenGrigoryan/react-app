import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";

// helpers
import { getCookie } from "../helpers/getCookie";

class Home extends Component {
    constructor() {
        super();

        this.state = {
            toLogin: false
        };
    }

    shouldComponentUpdate() {
        return true;
    }

    componentDidMount() {
        let isLogged = getCookie("isLogged");
        if (!isLogged || isLogged !== "true") {
            this.setState({
                toLogin: true
            });
        }
    }

    logOut = () => {
        this.setState({
            toLogin: true
        });
    };

    render() {
        if (this.state.toLogin === true) {
            return <Redirect to="/login" />;
        }
        const user = this.props.user;
        return (
            <>
                <div>
                    {user && user.userName && <h2> Welcome {user.userName}</h2>}
                    <button onClick={this.logOut}>Log out</button>
                </div>
                <div></div>
            </>
        );
    }
}

function mapStateToProps(state) {
    return {
        user: state.user
    };
}

export default connect(mapStateToProps)(Home);
