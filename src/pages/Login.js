import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

// Actions
import { addUserInfo, deleteUserInfo } from '../store/actions/index';

// Components
import Button from '../components/Button';
import Input from '../components/Input';

class Login extends Component {
    constructor(props) {
        super(props);

        this.state = {
            email: '',
            password: '',
            toHome: false
        }
    }

    componentDidMount() {
        document.cookie = 'isLogged = false';
        this.props.deleteUserInfo();
    }

    onSubmit = (e) => {
        e.preventDefault();
        const { email, password } = this.state;

        if ((email && password) && (email === 'test@gmail.com' && password === 'welcome123')) {
            const cookie = 'isLogged = true';            
            document.cookie = cookie;
            this.props.addUserInfo({ userName: 'Tom22' });
            this.setState({
                toHome: true
            });
        }
    }

    handleChange = (event) => {
        const {name, value} = event.target;
        this.setState({
            [name]: value
        });
    }

    render() {
        if (this.state.toHome === true) {
            return <Redirect to='/' />
        }
        return(
            <form onSubmit={this.onSubmit}>
                E-mail:<br />
                <Input 
                    value={this.state.email}
                    type={'text'}
                    name={'email'}
                    placeholder={'Please enter emial'}
                    handleChange={this.handleChange}
                />
                <br />
                Password:<br />
                <Input
                    value={this.state.password}
                    type={'password'}
                    name={'password'}
                    placeholder={'Please enter password'}
                    handleChange={this.handleChange}
                />                
                <br />
                <Button onSubmit={this.onSubmit} />
            </form> 
        )
    }
}

const mapDispatchToProps = dispatch => ({
    addUserInfo: info => dispatch(addUserInfo(info)),
    deleteUserInfo: () => dispatch(deleteUserInfo())
  })
export default connect(null, mapDispatchToProps)(Login);