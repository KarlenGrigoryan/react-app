export const addUserInfo = (info) => {
    return {
        type: 'ADD_USER_INFO',
        info
    }
}

export const deleteUserInfo = () => {
    return {
        type: 'DELETE_USER_INFO',
    }
}