const INITIAL_STATE = {
    user: {}
};

export const userReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case "ADD_USER_INFO":
            state.user = action.info;
            return { ...state };
        case "DELETE_USER_INFO":
            return (state = null);
        default:
            return state;
    }
};
