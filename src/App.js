import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

// Pages
import Login from "./pages/Login";
import Home from "./pages/Home";

function App() {
    return (
        <Router>
            <div className="App">
                <Switch>
                    <Route exact path="/" component={Home} />
                    <Route path="/login" component={Login}></Route>
                </Switch>
            </div>
        </Router>
    );
}

export default App;
