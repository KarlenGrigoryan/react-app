import React, { PureComponent } from 'react';

class Button extends PureComponent {
    render() {
        return <button type='submit' onClick={this.props.onSubmit}>Submit</button>
    }
}

export default Button;