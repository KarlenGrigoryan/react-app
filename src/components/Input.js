import React, { Component } from 'react';

class Input extends Component {
    render() {
        let inputValue = '';
        const { type, placeholder, name, email, password } = this.props;
        
        name === 'email' ? inputValue = email : inputValue = password;
        
        return <input
                    value={inputValue}
                    type={type}
                    name={name}
                    placeholder={placeholder}
                    onChange={this.props.handleChange}
                />
    }
}

export default Input;